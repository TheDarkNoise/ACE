 
namespace ACE.Server 
{ 
    public static partial class ServerBuildInfo 
    { 
        public static string Branch = "master"; 
        public static string Commit = "2120443af69b6046a7832eba7bdc1e46edca59b9"; 
 
        public static string Version = "1.52"; 
        public static string Build   = "4418"; 
 
        public static int BuildYear   = 2023; 
        public static int BuildMonth  = 07; 
        public static int BuildDay    = 28; 
        public static int BuildHour   = 17; 
        public static int BuildMinute = 24; 
        public static int BuildSecond = 29; 
    } 
} 
 
